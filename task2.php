<?php
$arr = array("hello", "world", "how", "are", "you");
$len = count($arr);

for ($i = $len - 1; $i >=0; $i--) {
    echo ucwords(strrev($arr[$i])) . " ";
}